from abc import ABC, abstractmethod
from typing import SupportsAbs

class Duck(ABC):

    @abstractmethod
    def quack(self):
        pass

    @abstractmethod
    def fly(self):
        pass

class MallardDuck(Duck):

    def quack(self):
        print('Quacking!')

    def fly(self):
        print('Flying!')

######################################################

class Drone(ABC):

    @abstractmethod
    def beep(self):
        pass

    @abstractmethod
    def spin_rotors(self):
        pass

    @abstractmethod
    def take_off(self):
        pass

class SuperDrone(Drone):

    def beep(self):
        print('Beeping!')

    def spin_rotors(self):
        print('Spinning!')

    def take_off(self):
        print('Taking off!')

######################################################

class DroneAdapter(Duck):

    def __init__(self, drone):
        super().__init__()
        self.drone = drone

    def quack(self):
        self.drone.beep()

    def fly(self):
        self.drone.spin_rotors()
        self.drone.take_off()

######################################################

class DuckSimulator:
   
    def testDuck(duck):

        duck.quack()
        duck.fly()

if __name__ == '__main__':

    duck = MallardDuck()
    DuckSimulator.testDuck(duck)

    print()

    drone = DroneAdapter(SuperDrone())
    DuckSimulator.testDuck(drone)
