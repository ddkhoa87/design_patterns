#include <iostream>

using namespace std;

class Duck
{
public:
    virtual void quack() = 0;
    virtual void fly() = 0;
};

class MallardDuck : public Duck
{
public:
    void quack()
    {
        cout << "Quacking!" << endl;
    }

    void fly()
    {
        cout << "Flying!" << endl;
    }
};

////////////////////////////////////////////////////

class DuckSimulator
{
public:
    static void testDuck(Duck *duck)
    {
        duck->quack();
        duck->fly();
    }
};

////////////////////////////////////////////////////

class Drone
{
public:
    virtual void beep() = 0;
    virtual void spin_rotors() = 0;
    virtual void take_off() = 0;
};

class SuperDrone : public Drone
{
public:
    void beep()
    {
        cout << "Beeping!" << endl;
    }

    void spin_rotors()
    {
        cout << "Spinning!" << endl;
    }

    void take_off()
    {
        cout << "Taking Off!" << endl;
    }
};

////////////////////////////////////////////////////

class DroneAdapter : public Duck
{
private:
    Drone* drone;

public:
    DroneAdapter(Drone *drone)
    {
        this->drone = drone;
    }

    ~DroneAdapter()
    {
        if (this->drone)
        {
            delete this->drone;
        }
    }

    void quack()
    {
        drone->beep();
    }

    void fly()
    {
        drone->spin_rotors();
        drone->take_off();
    }
};

////////////////////////////////////////////////////

int main()
{
    Duck *duck = new MallardDuck;
    DuckSimulator::testDuck(duck);
    delete duck;

    cout << endl;

    duck = new DroneAdapter(new SuperDrone);
    DuckSimulator::testDuck(duck);

    return 0;
}