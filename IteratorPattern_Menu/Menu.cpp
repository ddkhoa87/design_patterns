/*
Note 1: _MenuItem needs a default constuctor so that the PancakeHouseMenu can use the list initialization.
Note 2: The DinerMenuIterator constructor assigns a reference to the mMenuItems before the function body {}.
*/

#include <iostream>
#include <string>
#include <vector>

using namespace std;

typedef struct _MenuItem
{
    _MenuItem() {}
    _MenuItem(const string &menuItem) : mMenuItem(menuItem) {}
    string mMenuItem;
} MenuItem;

class Iterator
{
public:
    
    virtual bool hasNext() = 0;

    virtual MenuItem next() = 0;
};

class PancakeHouseIterator : public Iterator
{
public:

    PancakeHouseIterator(MenuItem * menuItems, const int &itemCount)
    {
        mMenuItems = menuItems;
        mItemCount = itemCount;
    }

    bool hasNext()
    {
        return mItemIndex < mItemCount;
    }

    MenuItem next()
    {
        // TODO: Add try-except or call hasNext to check first.

        MenuItem menuItem = mMenuItems[mItemIndex];
        ++mItemIndex;
        return menuItem;
    }

private:
    MenuItem * mMenuItems = nullptr;
    int mItemCount = 0;
    int mItemIndex = 0;
};

class DinerMenuIterator : public Iterator
{
public:

    DinerMenuIterator(vector<MenuItem> &menuItems) : mMenuItems(menuItems) {}

    bool hasNext()
    {
        return mItemIndex < mMenuItems.size();
    }

    MenuItem next()
    {
        // TODO: Add try-except or call hasNext to check first.

        MenuItem menuItem = mMenuItems[mItemIndex];
        ++mItemIndex;
        return menuItem;
    }

private:
    vector<MenuItem> &mMenuItems;
    size_t mItemIndex = 0;
};

////////////////////////////////////////////////////////////////////////////////

class Menu
{
public:

    virtual Iterator* createIterator() = 0;
};

class PancakeHouseMenu : public Menu
{
public:
    PancakeHouseMenu()
    {
        mMenuCount = 4;
        mMenuItems =  new MenuItem[mMenuCount]
                        {MenuItem("Onion"), MenuItem("Pepper"), MenuItem("Chesse"), MenuItem("Sausage")};
    }

    ~PancakeHouseMenu()
    {
        if (mMenuItems)
        {
            delete mMenuItems;
        }
    }

    Iterator* createIterator()
    {
        return new PancakeHouseIterator(mMenuItems, mMenuCount);
    }

private:
    MenuItem *mMenuItems = nullptr;
    int mMenuCount = 0;
};

class DinerMenu : public Menu
{
public:
    DinerMenu()
    {
        mMenuItems = vector<MenuItem>{MenuItem("Appetizer"), MenuItem("Main Course"), MenuItem("Dessert")};
    }

    Iterator* createIterator()
    {
        return new DinerMenuIterator(mMenuItems);
    }

private:

    vector<MenuItem> mMenuItems;
};

////////////////////////////////////////////////////////////////////////////////

class Cafe
{
public:

    Cafe()
    {
        mPancakeHouseMenuIter = mPancakeHouseMenu.createIterator();
        mDinerMenuIter = mDinerMenu.createIterator();
    }

    ~Cafe()
    {
        if (mPancakeHouseMenuIter)
        {
            delete mPancakeHouseMenuIter;
        }

        if (mDinerMenuIter)
        {
            delete mDinerMenuIter;
        }
    }

    static void printMenu(Iterator *iter)
    {
        while (iter->hasNext())
        {
            cout << iter->next().mMenuItem << endl;
        }
    }

    Iterator *mPancakeHouseMenuIter = nullptr;
    Iterator *mDinerMenuIter = nullptr;

private:

    PancakeHouseMenu mPancakeHouseMenu;
    DinerMenu mDinerMenu;
};

////////////////////////////////////////////////////////////////////////////////

int main()
{
    Cafe cafe;

    Cafe::printMenu(cafe.mPancakeHouseMenuIter);
    cout << endl;
    Cafe::printMenu(cafe.mDinerMenuIter);
    
    return 0;
}