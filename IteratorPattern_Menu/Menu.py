from abc import ABC, abstractclassmethod

class Iterator(ABC):

    @abstractclassmethod
    def hasNext():
        pass

    @abstractclassmethod
    def next():
        pass

class PancakeHouseIterator(Iterator):

    def __init__(self, menuItems) -> None:
        super().__init__()

        self.menuItems = menuItems
        self.itemIndex = 0

    def hasNext(self):
        return self.itemIndex < len(self.menuItems)

    def next(self):

        # TODO: Add try-except or call hasNext to check first.
        
        menuItem = self.menuItems[self.itemIndex]
        self.itemIndex += 1
        return menuItem

class DinerIterator(Iterator):

    def __init__(self, menuItems) -> None:
        super().__init__()

        self.menuItems = menuItems
        self.itemIndex = 0
        self.itemIter = iter(self.menuItems)

    def hasNext(self):
        return self.itemIndex < len(self.menuItems)

    def next(self):
        menuItem = next(self.itemIter)
        self.itemIndex += 1
        return menuItem

#################################################################

class Menu(ABC):

    @abstractclassmethod
    def createIterator(self):
        pass

class PancakeHouseMenu(Menu):

    def __init__(self) -> None:
        super().__init__()

        self.menuItems = ["Onion", "Pepper", "Chesse", "Sausage"]
        
    def createIterator(self):
        return PancakeHouseIterator(self.menuItems)

class DinerMenu(Menu):

    def __init__(self) -> None:
        super().__init__()

        self.menuItems = ("Appetizer", "Main Course", "Dessert")

    def createIterator(self):
        return DinerIterator(self.menuItems)

#################################################################

class Cafe():

    def __init__(self, pancakeHouseMenu, dinerMenu) -> None:
        
        self.pancakeHouseMenu = pancakeHouseMenu
        self.pancakeHouseMenuIter = pancakeHouseMenu.createIterator()
        
        self.dinerMenu = dinerMenu
        self.dinerMenuIter = dinerMenu.createIterator()

    def printMenu(iter):
        while iter.hasNext():
            menuItem = iter.next()
            print(menuItem)

if __name__ == '__main__':

    pancakeHouseMenu = PancakeHouseMenu()
    dinerMenu = DinerMenu()

    cafe = Cafe(pancakeHouseMenu, dinerMenu)  

    Cafe.printMenu(cafe.pancakeHouseMenuIter)
    print()
    Cafe.printMenu(cafe.dinerMenuIter)