from abc import ABC, abstractmethod

class Subject(ABC):

    @abstractmethod
    def registerObserver(self, observer):
        pass

    @abstractmethod
    def removeObserver(self, observer):
        pass

    @abstractmethod
    def notifyObservers(self):
        pass

class WeatherData:

    def __init__(self, temperature = 30, windspeed = 30, pressure = 40) -> None:
        self.temperature = temperature
        self.windspeed = windspeed
        self.pressure = pressure
    
    def toText(weatherData):
        return f'Temperature: {weatherData.temperature}. Windspeed: {weatherData.windspeed}. Pressure: {weatherData.pressure}'
        
class WeatherStation(Subject):

    def __init__(self) -> None:
        
        self.weatherData = WeatherData()
        self.setWeatherData(30, 40, 70)

        self.observerList = []

    def registerObserver(self, observer):
        self.observerList.append(observer)
        print(f'Registered a new observer: {observer.getID()}')

    def removeObserver(self, observer):
        for osvr in self.observerList:
            if osvr.getID() == observer.getID():
                self.observerList.remove(osvr)
                print(f'Removed the observer: {observer.getID()}')
                break
        
    def notifyObservers(self):
        for osvr in self.observerList:
            osvr.update(self.weatherData)

    def setWeatherData(self, temperature, windSpeed, pressure):
        self.weatherData.temperature = temperature
        self.weatherData.windSpeed = windSpeed
        self.weatherData.pressure = pressure

##########################################################################

class Observer(ABC):

    def __init__(self, ID) -> None:
        self.ID = ID

    def getID(self):
        return self.ID

    @abstractmethod
    def update(self, data):
        pass

class UserInterface(Observer):

    def __init__(self, ID) -> None:
        super().__init__(ID)

    def update(self, data):
        UserInterface.display(data)

    def display(data):
        print(f'Displaying data: {WeatherData.toText(data)}')

class Logger(Observer):

    def __init__(self, ID) -> None:
        super().__init__(ID)

    def update(self, data):
        Logger.log(data)

    def log(data):
        print(f'Logging data: {WeatherData.toText(data)}')

class AlertSystem(Observer):

    def __init__(self, ID) -> None:
        super().__init__(ID)

    def update(self, data):
        AlertSystem.alert(data)

    def alert(data):
        try:
            if int(data.temperature) > 35:
                print(f'Alerting: {WeatherData.toText(data)}')
            else:
                print(f'Normal data: {WeatherData.toText(data)}')
        except:
            print(f'Invalid data: {WeatherData.toText(data)}')

##########################################################################

if __name__ == '__main__':
    weatherStation = WeatherStation()
    ui = UserInterface('UI')
    logger = Logger('Logger')
    alertSystem = AlertSystem('AlertSystem')

    weatherStation.registerObserver(ui)
    weatherStation.registerObserver(logger)
    weatherStation.registerObserver(alertSystem)

    weatherStation.notifyObservers()

    weatherStation.removeObserver(logger)

    weatherStation.setWeatherData(40, 60, 70)
    weatherStation.notifyObservers()
