#include <iostream>
#include <string>
#include <vector>
#include <sstream>
using namespace std;

typedef struct _WeatherData
{
    int mTemperature = 30;
    int mWindSpeed = 30;
    int mPressure = 40;

    _WeatherData(const int &temperature = 30, const int &windspeed = 30, const int &pressure = 40)
    {
        mTemperature = temperature;
        mWindSpeed = windspeed;
        mPressure = pressure;
    }

    static string toText(const _WeatherData &weatherData)
    {
        stringstream ss;
        ss  << "Temperature: " << weatherData.mTemperature 
            << ". Windspeed: " << weatherData.mWindSpeed
            << ". Pressure: " << weatherData.mPressure;
        
        return ss.str();
    }
} WeatherData;

///////////////////////////////////////////////////////////////////

class Observer
{
private:
    string mID;

public:
    Observer(const string &id) : mID(id) {}
    
    const string& getID()
    {
        return mID;
    }

    virtual void update(const WeatherData &weatherData) = 0;
};

class UserInterface : public Observer
{
public:
    UserInterface() : Observer("UI") {}

    void update(const WeatherData &weatherData)
    {
        display(weatherData);
    }

private:
    void display(const WeatherData &weatherData)
    {
        cout << "Displaying data: " << WeatherData::toText(weatherData) << endl;
    }
};

class Logger : public Observer
{
public:
    Logger() : Observer("Logger") {}

    void update(const WeatherData &weatherData)
    {
        log(weatherData);
    }

private:
    void log(const WeatherData &weatherData)
    {
        cout << "Logging data: " << WeatherData::toText(weatherData) << endl;
    }
};

class AlertSystem : public Observer
{
public:
    AlertSystem() : Observer("AlertSystem") {}

    void update(const WeatherData &weatherData)
    {
        alert(weatherData);
    }

private:
    void alert(const WeatherData &weatherData)
    {
        if (weatherData.mTemperature > 35)
        {
            cout << "Alerting: " << WeatherData::toText(weatherData) << endl;
        }
        else
        {
            cout << "Normal data: " << WeatherData::toText(weatherData) << endl;
        }
    }
};

///////////////////////////////////////////////////////////////////

class Subject
{
public:
    
    virtual void registerObserver(Observer *observer) = 0;
    virtual void removeObserver(Observer *observer) = 0;
    virtual void notifyObservers() = 0;
};

class WeatherStation : public Subject
{
private:  

    WeatherData mWeatherData;

    vector<Observer*> mObserverList;

public:
    void registerObserver(Observer *observer)
    {
        mObserverList.push_back(observer);
        cout << "Registered a new observer: " << observer->getID() << endl;
    }

    void removeObserver(Observer *observer)
    {
        for (int i = mObserverList.size() - 1; i > -1; --i)
        {
            if (mObserverList[i]->getID() == observer->getID())
            {
                mObserverList.erase(mObserverList.begin() + i);
                cout << "Removed the observer: " << observer->getID() << endl;
            }
        }
    }

    void notifyObservers()
    {
        for (const auto &observer : mObserverList)
        {
            observer->update(mWeatherData);
        }
    }

    void setWeatherData(const WeatherData &weatherData)
    {
        mWeatherData = weatherData;
    }
};

///////////////////////////////////////////////////////////////////

int main() 
{
    WeatherStation weatherStation;
    Observer * ui = new UserInterface;
    Observer * logger = new Logger;
    Observer * alertSystem = new AlertSystem;

    weatherStation.registerObserver(ui);
    weatherStation.registerObserver(logger);
    weatherStation.registerObserver(alertSystem);

    weatherStation.notifyObservers();

    weatherStation.removeObserver(logger);

    weatherStation.setWeatherData(WeatherData(40, 50, 70));
    weatherStation.notifyObservers();

    delete ui;
    delete logger;
    delete alertSystem;

    return 0;
}