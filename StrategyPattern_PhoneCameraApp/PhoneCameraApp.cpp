#include <iostream>
using namespace std;

class ShareBehavior
{
public:
    virtual void share() = 0;
};

class ShareBySMS : public ShareBehavior
{
public:
    void share() override
    {
        cout << "Sharing by SMS!" << endl;
    }
};

class ShareByEmail : public ShareBehavior
{
public:
    void share() override
    {
        cout << "Sharing by Email!" << endl;
    }
};

////////////////////////////////////////////////////////

class PhoneCameraApp
{
protected:
    ShareBehavior *shareBehavior = nullptr;

    void setShareBehavior(ShareBehavior *behavior)
    {
        if (this->shareBehavior)
        {
            delete this->shareBehavior;
            this->shareBehavior = nullptr;
        }

        this->shareBehavior = behavior;
    }

public:
    ~PhoneCameraApp()
    {
        if (this->shareBehavior)
        {
            delete this->shareBehavior;
            this->shareBehavior = nullptr;
        }
    }

    void take()
    {
        cout << "Taking photo!" << endl;
    }

    void save()
    {
        cout << "Saving photo!" << endl;
    }

    void performShare()
    {
        shareBehavior->share();
    }

    virtual void edit() = 0;
};

class BasicCameraApp : public PhoneCameraApp
{
public:
    
    BasicCameraApp() : PhoneCameraApp()
    {
        setShareBehavior(new ShareBySMS);
    }

    void edit() override
    {
        cout << "Basic editing!" << endl;
    }
};

class CameraPlusApp : public PhoneCameraApp
{
public:
    CameraPlusApp() : PhoneCameraApp()
    {
        setShareBehavior(new ShareByEmail);
    }

    void edit() override
    {
        cout << "Plus editing!" << endl;
    }
};

////////////////////////////////////////////////////////

int main()
{
    PhoneCameraApp *cameraApp = new BasicCameraApp;
    cameraApp->take();
    cameraApp->edit();
    cameraApp->save();
    cameraApp->performShare();
    delete cameraApp;

    cout << endl;

    cameraApp = new CameraPlusApp;
    cameraApp->take();
    cameraApp->edit();
    cameraApp->save();
    cameraApp->performShare();
    delete cameraApp;

    return 0;
}