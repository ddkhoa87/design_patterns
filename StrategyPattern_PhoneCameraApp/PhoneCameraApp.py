from abc import ABC, abstractmethod

class ShareBehavior(ABC):

    @abstractmethod
    def share(self):
        pass

class ShareBySMS(ShareBehavior):

    def share(self):
        print('Sharing by SMS!')

class ShareByEmail(ShareBehavior):

    def share(self):
        print('Sharing by Email!')

################################################

class PhoneCameraApp(ABC):

    def __init__(self) -> None:
        super().__init__()

        self.shareBehavior = None

    def setShareBehavior(self, behavior):
        self.shareBehavior = behavior

    ##########################

    def take(self):
        print('Taking photo!')

    def save(self):
        print('Saving photo!')

    def performShare(self):
        self.shareBehavior.share()

    @abstractmethod
    def edit(self):
        pass

class BasicCameraApp(PhoneCameraApp):

    def __init__(self) -> None:
        super().__init__()

        self.setShareBehavior(ShareBySMS())

    def edit(self):
        print('Basic editing!')

class CameraPlusApp(PhoneCameraApp):

    def __init__(self) -> None:
        super().__init__()

        self.setShareBehavior(ShareByEmail())

    def edit(self):
        print('Plus editing!')

################################################

if __name__ == '__main__':

    cameraApp1 = BasicCameraApp()
    cameraApp1.take()
    cameraApp1.edit()
    cameraApp1.save()
    cameraApp1.performShare()

    print()

    cameraApp2 = CameraPlusApp()
    cameraApp2.take()
    cameraApp2.edit()
    cameraApp2.save()
    cameraApp2.performShare()