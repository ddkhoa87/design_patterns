from abc import ABC, abstractclassmethod

class Pizza(ABC):

    def __init__(self, description) -> None:
        self.description = description

    def getDescription(self):
        return self.description

    @abstractclassmethod
    def cost():
        pass

class ThinCrustPizza(Pizza):

    def __init__(self, description='Thin Crust Pizza') -> None:
        super().__init__(description)

    def cost(self):
        return 7

class ThickCrustPizza(Pizza):

    def __init__(self, description='Thick Crust Pizza') -> None:
        super().__init__(description)

    def cost(self):
        return 5

#######################################################################

class ToppingDecorator(Pizza):

    def __init__(self, pizza, description) -> None:
        super().__init__(description)

        self.pizza = pizza

    @abstractclassmethod
    def getDescription(self):
        pass

class Cheese(ToppingDecorator):

    def __init__(self, pizza, description='Cheese') -> None:
        super().__init__(pizza, description)

    def getDescription(self):
        return f'{self.pizza.getDescription()}, {self.description}'

    def cost(self):
        return self.pizza.cost() + 1

class Olives(ToppingDecorator):

    def __init__(self, pizza, description='Olives') -> None:
        super().__init__(pizza, description)

    def getDescription(self):
        return f'{self.pizza.getDescription()}, {self.description}'

    def cost(self):
        return self.pizza.cost() + 1.5

class Pepper(ToppingDecorator):

    def __init__(self, pizza, description='Pepper') -> None:
        super().__init__(pizza, description)

    def getDescription(self):
        return f'{self.pizza.getDescription()}, {self.description}'

    def cost(self):
        return self.pizza.cost() + 0.5

#######################################################################

if __name__ == '__main__':

    pizza = ThinCrustPizza()
    pizza = Olives(pizza)
    pizza = Pepper(pizza)

    print(pizza.getDescription(), pizza.cost())

    pizza = ThickCrustPizza()
    pizza = Cheese(pizza)
    pizza = Cheese(pizza)
    print(pizza.getDescription(), pizza.cost())