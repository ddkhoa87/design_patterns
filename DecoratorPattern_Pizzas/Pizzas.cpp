/*
Note 1: Sub classes of ToppingDecorator cannot call the function getDescription to get its own description.
Must use mDescription explicitly. Because these classes reimplement getDesciption in their own ways,
not the same as Pizza::getDescription().

Note2: Pizza::getDescription() must be virtual (not necessary pure virtual) for an object of 
a sub class of ToppingDecorator to be able to call its own getDescription().
*/

#include <iostream>
#include <string>
using namespace std;

class Pizza
{
public:
    Pizza(const string &description) : mDescription(description) {}
    
    virtual string getDescription()
    {
        return mDescription;
    }

    virtual float cost() = 0;

protected:
    string mDescription;
};

class ThinCrustPizza : public Pizza
{
public:
    ThinCrustPizza() : Pizza("Thin Crust Pizza") 
    {
        cout << "Add " << getDescription() << endl;
    }

    float cost()
    {
        return 7;
    }
};

class ThickCrustPizza : public Pizza
{
public:
    ThickCrustPizza() : Pizza("Thick Crust Pizza")
    {
        cout << "Add" << getDescription() << endl;
    }

    float cost()
    {
        return 5;
    }
};

////////////////////////////////////////////////////////////////////////

class ToppingDecorator : public Pizza
{
public:
    ToppingDecorator(Pizza * pizza, const string& description) : 
        Pizza(description), mPizza(pizza) {}

    virtual string getDescription() = 0;

protected:
    Pizza * mPizza;
};

class Cheese : public ToppingDecorator
{
public:
    Cheese(Pizza * pizza, const string &description = "Cheese") : ToppingDecorator(pizza, description)
    {
        cout << "Add " << mDescription << endl;
    }

    string getDescription()
    {
        return mPizza->getDescription() + ", " + mDescription;
    }

    float cost()
    {
        return mPizza->cost() + 1;
    }
};

class Olives : public ToppingDecorator
{
public:
    Olives(Pizza * pizza, const string& description = "Olives") : ToppingDecorator(pizza, description)
    {
        cout << "Add " << mDescription << endl;
    }

    string getDescription()
    {
        return mPizza->getDescription() + ", " + mDescription;
    }

    float cost()
    {
        return mPizza->cost() + 1.5f;
    }
};

class Pepper : public ToppingDecorator
{
public:
    Pepper(Pizza * pizza, const string& description = "Pepper") : ToppingDecorator(pizza, description)
    {
        cout << "Add " << mDescription << endl;
    }

    string getDescription()
    {
        return mPizza->getDescription() + ", " + mDescription;
    }

    float cost()
    {
        return mPizza->cost() + 0.5f;
    }
};

////////////////////////////////////////////////////////////////////////

int main()
{
    Pizza * pizza = new ThinCrustPizza();
    pizza = new Olives(pizza);
    pizza = new Pepper(pizza);

    cout << pizza->getDescription() << ", " << pizza->cost() << endl;

    delete pizza;

    pizza = new ThickCrustPizza();
    pizza = new Cheese(pizza);
    pizza = new Cheese(pizza);

    cout << pizza->getDescription() << ", " << pizza->cost() << endl;

    delete pizza;

    return 0;
}