from abc import ABC, abstractclassmethod
from enum import Enum

class Zone(ABC):

    def __init__(self, displayName, offset):
        self.displayName = displayName
        self.offset = offset

    def getDisplayName(self):
        return self.displayName

    def getOffset(self):
        return self.offset

class ZoneUSEastern(Zone):

    def __init__(self):
        super().__init__('US Eastern', -5)

class ZoneUSCentral(Zone):

    def __init__(self):
        super().__init__('US Central', -6)

class ZoneUSMountain(Zone):

    def __init__(self):
        super().__init__('US Mountain', -7)

class ZoneUSPacific(Zone):

    def __init__(self):
        super().__init__('US Pacific', -8)

####################################################

class ZoneID(Enum):
    US_EASTERN = -5
    US_CENTRAL = -6
    US_MOUNTAIN = -7
    US_PACIFIC = -8

class ZoneFactory():

    def CreateZone(zoneID):

        if zoneID == ZoneID.US_EASTERN:
            return ZoneUSEastern()
        elif zoneID == ZoneID.US_CENTRAL:
            return ZoneUSCentral()
        elif zoneID == ZoneID.US_MOUNTAIN:
            return ZoneUSCentral()
        elif zoneID == ZoneID.US_PACIFIC:
            return ZoneUSPacific()

####################################################

class Calendar(ABC):

    def __init__(self):
        self.zone = None

    def print(self):
        print(self.zone.getDisplayName(), self.zone.getOffset())

    @abstractclassmethod
    def createCalendar(self):
        pass

class PacificCalendar(Calendar):

    def createCalendar(self):
        self.zone = ZoneFactory.CreateZone(ZoneID.US_PACIFIC)

####################################################

if __name__ == '__main__':

   calendar = PacificCalendar()
   calendar.createCalendar()
   calendar.print() 