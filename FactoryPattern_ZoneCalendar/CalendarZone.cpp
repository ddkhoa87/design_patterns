#include <iostream>
#include <string>

using namespace std;

class Zone
{
public:
    Zone(string displayName, int offset) : 
        mDisplayName(displayName), 
        mOffset(offset)
        {}
    
    string getDisplayName()
    {
        return mDisplayName;
    }

    int getOffset()
    {
        return mOffset;
    }
protected:
    string mDisplayName;
    int mOffset;
};

class ZoneUSEastern : public Zone
{
public:
    ZoneUSEastern() : Zone("US Eastern", -5) {}
};

class ZoneUSCentral : public Zone
{
public:
    ZoneUSCentral() : Zone("US Central", -6) {}
};

class ZoneUSMountain : public Zone
{
public:
    ZoneUSMountain() : Zone("US Mountain", -7) {}
};

class ZoneUSPacific : public Zone
{
public:
    ZoneUSPacific() : Zone("US Pacific", -8) {}
};

////////////////////////////////////////////////////////

class ZoneFactory
{
public:

    enum ZoneID
    {
        US_EASTERN,
        US_CENTRAL,
        US_MOUNTAIN,
        US_PACIFIC
    };

    static Zone* CreateZone(ZoneID zoneID)
    {
        Zone *zone = nullptr;

        switch (zoneID)
        {
        case US_EASTERN:
            zone = new ZoneUSEastern();
            break;
        case US_CENTRAL:
            zone = new ZoneUSCentral();
            break;
        case US_MOUNTAIN:
            zone = new ZoneUSMountain();
            break;
        case US_PACIFIC:
            zone = new ZoneUSPacific();
            break;
        default:
            return nullptr;
            break;
        }

        return zone;
    }
};

////////////////////////////////////////////////////////

class Calendar
{
public:
    virtual void createCalendar() = 0;
    
    void print()
    {
        cout << mZone->getDisplayName() << ", " << mZone->getOffset() << endl;
    }
protected:
    Zone * mZone = nullptr;
};

class PacificCalendar : public Calendar
{
public:
    void createCalendar()
    {
        if (mZone)
        {
            delete mZone;
        }

        mZone = ZoneFactory::CreateZone(ZoneFactory::US_PACIFIC);
    }
};

////////////////////////////////////////////////////////

int main()
{
    Calendar * calendar = new PacificCalendar();
    calendar->createCalendar();
    calendar->print();
    
    return 0;
}